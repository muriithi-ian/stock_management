package com.stock.stock_management.model;

import javax.persistence.*;

@Entity
@Table(name = "category")
public class Stock_management {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "category_id")
    private long categoryId;

    @Column(name ="category_name")
    private String categoryName;

    @Column(name ="status")
    private boolean status;

    public Stock_management(String categoryName, boolean status) {
        this.categoryName = categoryName;
        this.status = status;
    }

    public long getcategoryId() {
        return categoryId;
    }

    public void setcategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public String getcategoryName() {
        return categoryName;
    }

    public void setcategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
