package com.stock.stock_management.controller;

import com.stock.stock_management.exception.ResourceNotFoundException;
import com.stock.stock_management.model.Stock_management;
import com.stock.stock_management.repository.Stock_managementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class Stock_managementController {

    @Autowired
    private Stock_managementRepository stock_managementRepository;

    // get categories
    @GetMapping("stock_management")
    public List<Stock_management> getAllCategories(){
        return this.stock_managementRepository.findAll();
    }
    //get category by id
    @GetMapping("Stock_management/{categoryID}")
public ResponseEntity<Stock_management> getCategoryById(@PathVariable(value = "category_id") long categoryId)
        throws ResourceNotFoundException {
        Stock_management stockManagement =stock_managementRepository.findById(categoryId)
                .orElseThrow(()-> new ResourceNotFoundException("Category not found for this Id::" +  categoryId));
        return ResponseEntity.ok().body(stockManagement);
}
    //save category
    @PostMapping("stock_management")
    public Stock_management createCategory(@RequestBody Stock_management category){
        return this.stock_managementRepository.save(category);
    }
    //update category
    @PutMapping("stock_management/{categoryId}")
    public ResponseEntity<Stock_management> updateCategory(@PathVariable(value = "category_id") long categoryId,@Valid @RequestBody Stock_management categoryDetails) throws ResourceNotFoundException {
        Stock_management stockManagement =stock_managementRepository.findById(categoryId)
                .orElseThrow(()-> new ResourceNotFoundException("Category not found for this Id::" +  categoryId));
        stockManagement.setcategoryName(categoryDetails.getcategoryName());
        stockManagement.setStatus(categoryDetails.isStatus());

        return ResponseEntity.ok(this.stock_managementRepository.save(categoryDetails));
    }

    // delete category
    @DeleteMapping("stock_management/{categoryId}")
    public Map<String, Boolean> deleteCategory(@PathVariable(value = "category_id") long categoryId) throws ResourceNotFoundException {
        Stock_management stockManagement =stock_managementRepository.findById(categoryId)
                .orElseThrow(()-> new ResourceNotFoundException("Category not found for this Id::" +  categoryId));
        this.stock_managementRepository.delete(stockManagement);

        Map<String, Boolean> response =  new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
