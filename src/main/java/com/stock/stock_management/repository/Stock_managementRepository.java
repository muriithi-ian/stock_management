package com.stock.stock_management.repository;

import com.stock.stock_management.model.Stock_management;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Stock_managementRepository extends JpaRepository<Stock_management, Long> {

}
